INSERT INTO car_schema.car(id, registration_number, brand, car_type, price)
VALUES (1001, '12345', 'BMW', '420i', 43299.99);

INSERT INTO car_schema.car(id, registration_number, brand, car_type, price)
VALUES (1002, '12346', 'Renault', 'Clio', 33250.99);

INSERT INTO car_schema.car(id, registration_number, brand, car_type, price)
VALUES (1003, '12347', 'Toyota', 'Prius', 24299.99);

INSERT INTO car_schema.car(id, registration_number, brand, car_type, price)
VALUES (1004, '12348', 'Audi', 'TT', 40000.00);

INSERT INTO car_schema.car(id, registration_number, brand, car_type, price)
VALUES (1005, '12349', 'Seat', 'Arosa', 14999.99);
