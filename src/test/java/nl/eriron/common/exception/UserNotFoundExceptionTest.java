package nl.eriron.common.exception;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserNotFoundExceptionTest {

    @Test
    public void testConstructor() {

        Long id = 13L;
        String entityName = "user";
        Object[] arguments = new Object[] { "bla1", 10L};

        EntityNotFoundException objectUnderTest = new EntityNotFoundException(id, entityName, arguments);

        assertEquals(id, objectUnderTest.getId());
        assertEquals(entityName, objectUnderTest.getEntityName());
        assertEquals(arguments.length, objectUnderTest.getArguments().length);
        assertEquals(arguments[0], objectUnderTest.getArguments()[0]);
        assertEquals(arguments[1], objectUnderTest.getArguments()[1]);
    }

    @Test
    public void testConstructorNoArguments() {

        Long id = 13L;
        String entityName = "user";

        EntityNotFoundException objectUnderTest = new EntityNotFoundException(id, entityName);

        assertEquals(id, objectUnderTest.getId());
        assertEquals(entityName, objectUnderTest.getEntityName());
        assertEquals(0, objectUnderTest.getArguments().length);
    }
}