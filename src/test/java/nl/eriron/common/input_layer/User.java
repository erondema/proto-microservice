package nl.eriron.common.input_layer;

/**
 * Class used for testing purposes.
 *
 * @author Erik Rondema
 */
@SuppressWarnings("unused")
class User {

    private String username;
    private String password;

    User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
