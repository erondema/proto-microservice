package nl.eriron.common.input_layer;

import nl.eriron.common.exception.EntityNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CommonRestExceptionHandlerTest {

    private static final Locale LOCALE = new Locale("nl");

    private CommonRestExceptionHandler objectUnderTest;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        LocaleContextHolder.setDefaultLocale(LOCALE);

        CommonInputLayerConfiguration configuration = new CommonInputLayerConfiguration();
        objectUnderTest = new CommonRestExceptionHandler();
        objectUnderTest.setMessageSource(configuration.messageSource());
    }

    @Test
    public void testAnnotationOnClass() {
        assertNotNull(objectUnderTest.getClass().getAnnotation(RestControllerAdvice.class));
    }

    @Test
    public void testAnnotationsOnHandleNotValid() throws NoSuchMethodException {

        Method handleNotValidMethod = objectUnderTest.getClass().getMethod("handleNotValid",
                                                                           MethodArgumentNotValidException.class);

        ExceptionHandler exceptionHandlerAnnotation = handleNotValidMethod.getAnnotation(ExceptionHandler.class);
        assertEquals(MethodArgumentNotValidException.class, exceptionHandlerAnnotation.value()[0]);

        ResponseStatus responseStatusAnnotation = handleNotValidMethod.getAnnotation(ResponseStatus.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusAnnotation.value());
    }

    @Test
    public void testAnnotationsOnHandleUserNotFound() throws NoSuchMethodException {

        Method handleUserNotFoundMethod = objectUnderTest.getClass().getMethod("handleEntityNotFound",
                                                                               EntityNotFoundException.class);

        ExceptionHandler exceptionHandlerAnnotation = handleUserNotFoundMethod.getAnnotation(ExceptionHandler.class);
        assertEquals(EntityNotFoundException.class, exceptionHandlerAnnotation.value()[0]);

        ResponseStatus responseStatusAnnotation = handleUserNotFoundMethod.getAnnotation(ResponseStatus.class);
        assertEquals(HttpStatus.NOT_FOUND, responseStatusAnnotation.value());
    }

    @Test
    public void testAnnotationsOnHandleException() throws NoSuchMethodException {

        Method handleUserNotFoundMethod = objectUnderTest.getClass().getMethod("handleException",
                                                                               Exception.class);

        ExceptionHandler exceptionHandlerAnnotation = handleUserNotFoundMethod.getAnnotation(ExceptionHandler.class);
        assertEquals(Exception.class, exceptionHandlerAnnotation.value()[0]);

        ResponseStatus responseStatusAnnotation = handleUserNotFoundMethod.getAnnotation(ResponseStatus.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseStatusAnnotation.value());
    }

    @Test
    public void testHandleNotValid() {

        User entity = new User("jbond", "shhht");
        BindingResult bindingResult = new BeanPropertyBindingResult(entity, "user");
        bindingResult.rejectValue("username", "user.username.too.long", new Object[] { "50" }, null);
        bindingResult.rejectValue("password", "user.password.empty");
        bindingResult.reject("user.date2.before.date1");

        MethodArgumentNotValidException exception = new MethodArgumentNotValidException(null, bindingResult);

        long now = System.currentTimeMillis();
        ErrorResponse result = objectUnderTest.handleNotValid(exception);

        assertNotNull(result);
        assertTrue(result.getTimestamp() - now < 60000);
        assertEquals("Bad Request", result.getStatus());
        assertEquals("Object: 'user' Field: 'password' Message: het wachtwoord mag niet leeg zijn", result.getMessage());

        List<ErrorResponse.Error> erros = result.getErrors();
        assertEquals(3, erros.size());
        assertEquals("user", erros.get(0).getObjectName());
        assertNull(erros.get(0).getField());
        assertEquals("datum 2 mag niet voor datum 1 liggen", erros.get(0).getMessage());
        assertEquals("user", erros.get(1).getObjectName());
        assertEquals("username", erros.get(1).getField());
        assertEquals("de gebruikersnaam mag niet langer zijn dan 50 karakters", erros.get(1).getMessage());
        assertEquals("user", erros.get(2).getObjectName());
        assertEquals("password", erros.get(2).getField());
        assertEquals("het wachtwoord mag niet leeg zijn", erros.get(2).getMessage());
    }

    @Test
    public void testHandEntityNotFound() {

        Long id = 5345233L;
        String entityName = "user";
        Object[] arguments = new Object[] { "jbond" };

        EntityNotFoundException exception = new EntityNotFoundException(id, entityName, arguments);

        ErrorResponse result = objectUnderTest.handleEntityNotFound(exception);

        assertNotNull(result);
        assertEquals("Not Found", result.getStatus());
        assertEquals("de gebruiker met ID 5345233 en gebruikersnaam jbond is niet gevonden", result.getMessage());
        assertTrue(result.getErrors().isEmpty());
    }

    @Test
    public void testHandleOptimisticLockingError() {

        Long id = 212312L;
        ObjectOptimisticLockingFailureException exception = new ObjectOptimisticLockingFailureException(User.class, id);

        ErrorResponse result = objectUnderTest.handleOptimisticLockingError(exception);

        assertNotNull(result);
        assertEquals("Precondition Failed", result.getStatus());
        assertEquals("de gebruiker met ID 212312 is veranderd door een andere gebruiker", result.getMessage());
    }

    @Test
    public void testHandle() {

        IllegalArgumentException exception = new IllegalArgumentException("Oops");

        ErrorResponse result = objectUnderTest.handleException(exception);

        assertNotNull(result);
        assertEquals("Internal Server Error", result.getStatus());
        assertEquals("Er is iets fout gegaan op de server", result.getMessage());
        assertTrue(result.getErrors().isEmpty());
    }

}