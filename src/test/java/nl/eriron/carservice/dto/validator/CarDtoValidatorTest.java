package nl.eriron.carservice.dto.validator;

import nl.eriron.carservice.dto.CarDto;
import nl.eriron.carservice.input_layer.InputLayerConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BindException;

import java.math.BigDecimal;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CarDtoValidatorTest {

    private CarDtoValidator objectUnderTest;

    @Before
    public void setup() {

        InputLayerConfiguration configuration = new InputLayerConfiguration();
        objectUnderTest = configuration.carDtoValidator();
    }

    @Test
    public void testSupports() {

        assertTrue(objectUnderTest.supports(CarDto.class));
        assertFalse(objectUnderTest.supports(String.class));
    }

    @Test
    public void testValidateWithValidNewCar() {

        CarDto car = createCarDto();
        car.setId(null);
        car.setRegistrationNumber(createLongString(30));
        car.setBrand(createLongString(255));
        car.setType(createLongString(255));
        BindException errors = new BindException(car, "car");

        objectUnderTest.validate(car, errors);

        assertFalse(errors.hasErrors());
    }

    @Test
    public void testValidateWithValidExistingCar() {

        CarDto car = createCarDto();
        car.setRegistrationNumber(createLongString(30));
        car.setBrand(createLongString(255));
        car.setType(createLongString(255));
        BindException errors = new BindException(car, "car");

        objectUnderTest.validate(car, errors);

        assertFalse(errors.hasErrors());
    }

    @Test
    public void testValidateWithoutBrand() {

        CarDto car = createCarDto();
        car.setBrand(null);
        BindException errors = new BindException(car, "car");

        objectUnderTest.validate(car, errors);

        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getErrorCount());
        assertTrue(errors.hasFieldErrors("brand"));
        assertEquals("car.brand.empty", Objects.requireNonNull(errors.getFieldError("brand")).getCode());
    }

    @Test
    public void testValidateWithTooLongName() {

        CarDto car = createCarDto();
        car.setBrand(createLongString(256));
        BindException errors = new BindException(car, "car");

        objectUnderTest.validate(car, errors);

        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getErrorCount());
        assertTrue(errors.hasFieldErrors("brand"));
        assertEquals("car.brand.too.long", Objects.requireNonNull(errors.getFieldError("brand")).getCode());
    }

    @Test
    public void testValidateWithoutPrice() {

        CarDto car = createCarDto();
        car.setPrice(null);
        BindException errors = new BindException(car, "car");

        objectUnderTest.validate(car, errors);

        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getErrorCount());
        assertTrue(errors.hasFieldErrors("price"));
        assertEquals("car.price.empty", Objects.requireNonNull(errors.getFieldError("price")).getCode());
    }

    @Test
    public void testValidateWithoutType() {

        CarDto car = createCarDto();
        car.setType(null);
        BindException errors = new BindException(car, "car");

        objectUnderTest.validate(car, errors);

        assertTrue(errors.hasErrors());
        assertEquals(1, errors.getErrorCount());
        assertTrue(errors.hasFieldErrors("type"));
        assertEquals("car.type.empty", Objects.requireNonNull(errors.getFieldError("type")).getCode());
    }

    private CarDto createCarDto() {

        CarDto dto = new CarDto();
        dto.setId(3L);
        dto.setRegistrationNumber("1234567890");
        dto.setBrand("BMW");
        dto.setType("420i");
        dto.setPrice(new BigDecimal("43299.99"));

        return dto;
    }

    private String createLongString(int length) {

        StringBuilder builder = new StringBuilder();
        for (int i=0; i < length; i++) {
            builder.append(i % 10);
        }

        return builder.toString();
    }
}