package nl.eriron.carservice.external_layer;

import com.zaxxer.hikari.HikariDataSource;
import nl.eriron.carservice.external_layer.db.DatabaseProperties;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExternalLayerConfigurationTest {

    private ExternalLayerConfiguration objectUnderTest;

    @Before
    public void setup() {
        objectUnderTest = new ExternalLayerConfiguration();
    }

    @Test
    public void testDataSource() {

        DatabaseProperties properties = new DatabaseProperties();
        properties.setDatabaseUrl("jdbc:postgresql://localhost:5432/postgres");
        properties.setDatabaseUsername("postgres");
        properties.setDatabasePassword("shhht!");
        properties.setDriverClassName("org.postgresql.Driver");

        HikariDataSource dataSource = (HikariDataSource) objectUnderTest.dataSource(properties);

        assertEquals(properties.getDatabaseUrl(), dataSource.getJdbcUrl());
        assertEquals(properties.getDatabaseUsername(), dataSource.getUsername());
        assertEquals(properties.getDatabasePassword(), dataSource.getPassword());
        assertEquals(properties.getDriverClassName(), dataSource.getDriverClassName());
    }
}