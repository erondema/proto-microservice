package nl.eriron.carservice.external_layer.db;

import nl.eriron.carservice.domain.Car;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DbConfig.class})
@ActiveProfiles("TEST")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:test_data.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, statements = "DELETE FROM car_schema.car")
public class CarRepositoryTest {

    @Autowired
    private CarRepository objectUnderTest;

    @BeforeClass
    public static void startContainer() {
        DbConfig.startContainer();
    }

    @AfterClass
    public static void stopContainer() {
        DbConfig.stopContainer();
    }

    @Test
    public void testSave() {

        Car car = new Car();
        car.setRegistrationNumber("1234567890");
        car.setBrand("BMW");
        car.setType("M2");
        car.setPrice(new BigDecimal("65347.30"));

        Car saveCar = objectUnderTest.save(car);

        assertNotNull(saveCar);
        assertNotNull(saveCar.getId());

        Optional<Car> result = objectUnderTest.findById(saveCar.getId());
        assertTrue(result.isPresent());
        Car foundCar = result.get();
        assertEquals(car.getRegistrationNumber(), foundCar.getRegistrationNumber());
        assertEquals(car.getBrand(), foundCar.getBrand());
        assertEquals(car.getPrice(), foundCar.getPrice());
        assertEquals(car.getType(), foundCar.getType());
    }

    @Test
    public void testFindByBrandContaining() {

        Iterable<Car> result = objectUnderTest.findByBrandContainingOrderByBrandAsc("a");

        Iterator<Car> iterator = result.iterator();

        Car car = iterator.next();
        assertEquals(new Long(1002L), car.getId());
        assertEquals("12346", car.getRegistrationNumber());
        assertEquals("Renault", car.getBrand());
        assertEquals("Clio", car.getType());
        assertEquals(new BigDecimal("33250.99"), car.getPrice());

        assertEquals(new Long(1005L), iterator.next().getId());
        assertEquals(new Long(1003L), iterator.next().getId());
    }
}