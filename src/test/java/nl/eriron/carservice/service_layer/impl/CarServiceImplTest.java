package nl.eriron.carservice.service_layer.impl;

import nl.eriron.carservice.domain.Car;
import nl.eriron.carservice.dto.CarDto;
import nl.eriron.carservice.dto.NewCarDto;
import nl.eriron.carservice.external_layer.db.CarRepository;
import nl.eriron.carservice.service_layer.ServiceLayerConfiguration;
import nl.eriron.carservice.service_layer.mapper.CarMapper;
import nl.eriron.common.exception.EntityNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceImplTest {

    private CarServiceImpl objectUnderTest;

    @Mock
    private CarRepository carRepository;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        ServiceLayerConfiguration configuration = new ServiceLayerConfiguration();
        CarMapper mapper = configuration.carMapper();
        objectUnderTest = (CarServiceImpl) configuration.carService(carRepository, mapper);
    }

    @Test
    public void testCreateCar() {

        Car domain = createDomain();
        when(carRepository.save(any(Car.class))).thenReturn(domain);

        CarDto result = objectUnderTest.createCar(createNewCarDto());

        verify(carRepository, times(1)).save(any(Car.class));
        assertEquals(domain.getId(), result.getId());
        assertEquals(domain.getRegistrationNumber(), result.getRegistrationNumber());
        assertEquals(domain.getBrand(), result.getBrand());
        assertEquals(domain.getType(), result.getType());
        assertEquals(domain.getPrice(), result.getPrice());
    }

    @Test
    public void testUpdateCar() {

        CarDto dto = createCarDto();

        Car domain = new Car();
        when(carRepository.findById(dto.getId())).thenReturn(Optional.of(domain));

        objectUnderTest.updateCar(dto.getId(), dto);

        ArgumentCaptor<Car> carArgumentCaptor = ArgumentCaptor.forClass(Car.class);
        verify(carRepository, times(1)).save(carArgumentCaptor.capture());
        assertEquals(dto.getId(), carArgumentCaptor.getValue().getId());
        assertEquals(dto.getRegistrationNumber(), carArgumentCaptor.getValue().getRegistrationNumber());
        assertEquals(dto.getBrand(), carArgumentCaptor.getValue().getBrand());
        assertEquals(dto.getType(), carArgumentCaptor.getValue().getType());
        assertEquals(dto.getPrice(), carArgumentCaptor.getValue().getPrice());
    }

    @Test
    public void testUpdateCarNotFound() {

        CarDto dto = createCarDto();
        Long id = dto.getId();
        when(carRepository.findById(id)).thenReturn(Optional.empty());

        try {
            objectUnderTest.updateCar(id, dto);
            fail("EntityNotFoundException expected");
        }
        catch (EntityNotFoundException enfe) {
            assertEquals(id, enfe.getId());
            assertEquals("car", enfe.getEntityName());
            verify(carRepository, times(0)).save(any(Car.class));
        }
    }

    @Test
    public void testDeleteCar() {

        Long id = 1234L;

        objectUnderTest.deleteCar(id);

        verify(carRepository, times(1)).deleteById(id);
    }

    @Test
    public void testDeleteCarNotFound() {

        Long id = 1234L;
        EmptyResultDataAccessException exception = new EmptyResultDataAccessException("", 1);
        doThrow(exception).when(carRepository).deleteById(id);

        try {
            objectUnderTest.deleteCar(id);
        }
        catch (EntityNotFoundException enfe) {
            assertEquals(id, enfe.getId());
            assertEquals("car", enfe.getEntityName());
        }
    }

    @Test
    public void testGetCar() {

        Car car1 = createDomain();
        car1.setId(1L);
        Car car2 = createDomain();
        car2.setId(2L);

        when(carRepository.findAll()).thenReturn(Arrays.asList(car1, car2));

        List<CarDto> result = objectUnderTest.getCars();

        assertEquals(2, result.size());
        assertEquals(car1.getId(), result.get(0).getId());
        assertEquals(car2.getId(), result.get(1).getId());
    }

    @Test
    public void testFindCarsByBrandContaining() {

        String text = "bla";

        Car car1 = createDomain();
        car1.setId(1L);
        Car car2 = createDomain();
        car2.setId(2L);

        when(carRepository.findByBrandContainingOrderByBrandAsc(text)).thenReturn(Arrays.asList(car1, car2));

        List<CarDto> result = objectUnderTest.findCarsByBrandContaining(text);

        assertEquals(2, result.size());
        assertEquals(car1.getId(), result.get(0).getId());
        assertEquals(car2.getId(), result.get(1).getId());
    }

    private NewCarDto createNewCarDto() {

        NewCarDto dto = new NewCarDto();
        dto.setRegistrationNumber("12345");
        dto.setBrand("BMW");
        dto.setType("420i");
        dto.setPrice(new BigDecimal("43999.99"));

        return dto;
    }

    private CarDto createCarDto() {

        CarDto dto = new CarDto();
        dto.setId(3L);
        dto.setRegistrationNumber("12345");
        dto.setBrand("BMW");
        dto.setType("420i");
        dto.setPrice(new BigDecimal("43999.99"));

        return dto;
    }

    private Car createDomain() {

        Car domain = new Car();
        domain.setId(3L);
        domain.setRegistrationNumber("12345");
        domain.setBrand("BMW");
        domain.setType("420i");
        domain.setPrice(new BigDecimal("43999.99"));

        return domain;
    }
}