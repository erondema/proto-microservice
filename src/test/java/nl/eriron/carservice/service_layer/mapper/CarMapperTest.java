package nl.eriron.carservice.service_layer.mapper;

import nl.eriron.carservice.domain.Car;
import nl.eriron.carservice.dto.CarDto;
import nl.eriron.carservice.service_layer.ServiceLayerConfiguration;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CarMapperTest {

    private CarMapper objectUnderTest;

    @Before
    public void setup() {

        ServiceLayerConfiguration configuration = new ServiceLayerConfiguration();
        objectUnderTest = configuration.carMapper();
    }

    @Test
    public void testConvertDomainToDto() {

        Car domain = new Car();
        domain.setId(3L);
        domain.setRegistrationNumber("12345");
        domain.setBrand("BMW");
        domain.setType("420i");
        domain.setPrice(new BigDecimal("43999.99"));

        CarDto dto = objectUnderTest.convert(domain);

        assertEquals(domain.getId(), dto.getId());
        assertEquals(domain.getRegistrationNumber(), dto.getRegistrationNumber());
        assertEquals(domain.getBrand(), dto.getBrand());
        assertEquals(domain.getPrice(), dto.getPrice());
        assertEquals(domain.getType(), dto.getType());
    }

    @Test
    public void testConvertDtoToDomain() {

        CarDto dto = new CarDto();
        dto.setId(3L);
        dto.setRegistrationNumber("12345");
        dto.setBrand("BMW");
        dto.setType("420i");
        dto.setPrice(new BigDecimal("43999.99"));

        Car domain = objectUnderTest.convert(dto);

        assertEquals(dto.getId(), domain.getId());
        assertEquals(dto.getRegistrationNumber(), domain.getRegistrationNumber());
        assertEquals(dto.getBrand(), domain.getBrand());
        assertEquals(dto.getPrice(), domain.getPrice());
        assertEquals(dto.getType(), domain.getType());
    }
}