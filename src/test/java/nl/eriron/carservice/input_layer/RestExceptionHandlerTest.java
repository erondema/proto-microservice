package nl.eriron.carservice.input_layer;

import nl.eriron.carservice.input_layer.rest.RestExceptionHandler;
import nl.eriron.common.input_layer.CommonInputLayerConfiguration;
import nl.eriron.common.input_layer.ErrorResponse;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;

import java.sql.SQLException;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class RestExceptionHandlerTest {

    private static final Locale LOCALE = new Locale("nl");

    private RestExceptionHandler objectUnderTest;

    @Before
    public void setup() {

        LocaleContextHolder.setDefaultLocale(LOCALE);

        CommonInputLayerConfiguration configuration = new CommonInputLayerConfiguration();
        objectUnderTest = new RestExceptionHandler();
        objectUnderTest.setMessageSource(configuration.messageSource());
    }

    @Test
    public void testHandleDataIntegrityViolationExceptionWithCustomerIdAlreadyExists() {

        String sqlErrorMessage = "ERROR: duplicate key value violates unique constraint \"car_registration_number_key\"\n" +
                " Detail: Key (registration_number)=(12345) already exists.";
        SQLException sqlException = new SQLException(sqlErrorMessage);
        ConstraintViolationException cve = new ConstraintViolationException("bla bla",
                                                                            sqlException,
                                                                            "car_registration_number_key");
        DataIntegrityViolationException dive = new DataIntegrityViolationException("bla", cve);

        ErrorResponse response = objectUnderTest.handleDataIntegrityViolationException(dive);

        assertEquals("Bad Request", response.getStatus());
        assertEquals("auto met registratie nummer 12345 wordt al gebruikt", response.getMessage());
    }

    @Test
    public void testHandleDataIntegrityViolationExceptionWithUnknownConstraint() {

        String sqlErrorMessage = "ERROR: bla bla bla";
        SQLException sqlException = new SQLException(sqlErrorMessage);
        ConstraintViolationException cve = new ConstraintViolationException("bla bla",
                                                                            sqlException,
                                                                            "bla_bla");
        DataIntegrityViolationException dive = new DataIntegrityViolationException("bla", cve);

        ErrorResponse response = objectUnderTest.handleDataIntegrityViolationException(dive);

        assertEquals("Bad Request", response.getStatus());
        assertEquals("de data kon niet naar de database geschreven worden", response.getMessage());
    }

    @Test
    public void testHandleDataIntegrityViolationExceptionWithDifferentMessage() {

        String sqlErrorMessage = "ERROR: bla bla.";
        SQLException sqlException = new SQLException(sqlErrorMessage);
        ConstraintViolationException cve = new ConstraintViolationException("bla bla",
                                                                            sqlException,
                                                                            "car_registration_number_key");
        DataIntegrityViolationException dive = new DataIntegrityViolationException("bla", cve);

        ErrorResponse response = objectUnderTest.handleDataIntegrityViolationException(dive);

        assertEquals("Bad Request", response.getStatus());
        assertEquals("auto met registratie nummer null wordt al gebruikt", response.getMessage());
    }
}