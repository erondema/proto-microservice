package nl.eriron.carservice.input_layer.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.eriron.carservice.dto.CarDto;
import nl.eriron.carservice.dto.NewCarDto;
import nl.eriron.carservice.service_layer.CarService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CarRestController.class)
public class CarRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CarService carService;

    private ObjectMapper objectMapper;

    @Before
    public void setup() {

        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Test
    public void testCreateCar() throws Exception {

        NewCarDto newCarDto = createNewCarDto();
        String json = objectMapper.writeValueAsString(newCarDto);

        CarDto carDto = createCarDto();
        when(carService.createCar(any(NewCarDto.class))).thenReturn(carDto);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/cars");
        builder.content(json);
        builder.contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(201, response.getStatus());
        assertEquals(objectMapper.writeValueAsString(carDto), response.getContentAsString());

        ArgumentCaptor<NewCarDto> newCarCaptor = ArgumentCaptor.forClass(NewCarDto.class);
        verify(carService, times(1)).createCar(newCarCaptor.capture());
        assertEquals(newCarDto.getBrand(), newCarCaptor.getValue().getBrand());
        assertEquals(newCarDto.getType(), newCarCaptor.getValue().getType());
        assertEquals(newCarDto.getPrice(), newCarCaptor.getValue().getPrice());
    }

    @Test
    public void testUpdateCar() throws Exception {

        Long id = 12345L;
        CarDto car = createCarDto();
        car.setId(id);
        String json = objectMapper.writeValueAsString(car);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/cars/" + id);
        builder.content(json);
        builder.contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());

        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<CarDto> carCaptor = ArgumentCaptor.forClass(CarDto.class);
        verify(carService, times(1)).updateCar(idCaptor.capture(), carCaptor.capture());
        assertEquals(id, idCaptor.getValue());
        assertEquals(car.getId(), carCaptor.getValue().getId());
        assertEquals(car.getBrand(), carCaptor.getValue().getBrand());
        assertEquals(car.getType(), carCaptor.getValue().getType());
        assertEquals(car.getPrice(), carCaptor.getValue().getPrice());
    }

    @Test
    public void testDeleteCar() throws Exception {

        Long id = 12345L;

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/cars/" + id);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());

        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);
        verify(carService, times(1)).deleteCar(idCaptor.capture());
        assertEquals(id, idCaptor.getValue());
    }

    @Test
    public void testGetCars() throws Exception {

        CarDto car = createCarDto();

        List<CarDto> carList = Collections.singletonList(car);
        when(carService.getCars()).thenReturn(carList);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/cars");
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());
        assertEquals(objectMapper.writeValueAsString(carList), response.getContentAsString());
    }

    @Test
    public void testFindCarsByNameContaining() throws Exception {

        String text = "benz";
        CarDto car = createCarDto();
        List<CarDto> carList = Collections.singletonList(car);

        ArgumentCaptor<String> textCaptor = ArgumentCaptor.forClass(String.class);
        when(carService.findCarsByBrandContaining(textCaptor.capture())).thenReturn(carList);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/cars");
        builder.param("text", text);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());
        assertEquals(objectMapper.writeValueAsString(carList), response.getContentAsString());
    }

    private NewCarDto createNewCarDto() {

        NewCarDto dto = new NewCarDto();
        dto.setBrand("BMW");
        dto.setType("420i");
        dto.setPrice(new BigDecimal("43299.99"));

        return dto;
    }

    private CarDto createCarDto() {

        CarDto dto = new CarDto();
        dto.setId(3L);
        dto.setBrand("BMW");
        dto.setType("420i");
        dto.setPrice(new BigDecimal("43299.99"));

        return dto;
    }
}