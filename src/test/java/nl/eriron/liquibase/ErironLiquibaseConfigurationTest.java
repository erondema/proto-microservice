package nl.eriron.liquibase;

import com.zaxxer.hikari.HikariDataSource;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ErironLiquibaseConfigurationTest {

    private ErironLiquibaseConfiguration fixture;

    @Before
    public void setup() {

        fixture = new ErironLiquibaseConfiguration();
    }

    @Test
    public void testDataSource() {

        ErironLiquibaseProperties properties = new ErironLiquibaseProperties();
        properties.setDatabaseUrl("jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = hand07)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = hand08)(PORT = 1521))(LOAD_BALANCE = yes)(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = trans)(FAILOVER_MODE =(TYPE = ELECT)(METHOD = BASIC)(RETRIES = 180)(DELAY = 5))))");
        properties.setDatabaseUsername("blaUserName");
        properties.setDatabasePassword("blaPassword");
        properties.setDriverClassName("org.hsqldb.jdbc.JDBCDriver");

        HikariDataSource dataSource = (HikariDataSource) fixture.dataSource(properties);
        assertEquals(properties.getDatabaseUsername(), dataSource.getUsername());
        assertEquals(properties.getDatabasePassword(), dataSource.getPassword());
        assertEquals(properties.getDriverClassName(), dataSource.getDriverClassName());
        assertEquals(properties.getDatabaseUrl(), dataSource.getJdbcUrl());
    }
}