package nl.eriron.liquibase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ErironLiquibaseDaoTest {

    private ErironLiquibaseDao fixture;
    private EmbeddedDatabase db;
    private JdbcTemplate jdbcTemplate;
    private String schema;

    @Before
    public void setup() {

        schema = "BLA";
        db = new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.HSQL)
            .addScript("schema.sql")
            .build();

        ErironLiquibaseConfiguration erironLiquibaseConfiguration = new ErironLiquibaseConfiguration();
        jdbcTemplate = erironLiquibaseConfiguration.jdbcTemplate(db);

        ErironLiquibaseProperties properties = new ErironLiquibaseProperties();
        properties.setChangelogFilename("changelogs/test-changelog-master.xml");

        fixture = erironLiquibaseConfiguration.erironLiquibaseDao(jdbcTemplate, properties);
    }

    @After
    public void cleanup() {
        db.shutdown();
    }

    @Test
    public void testGetCurrentTagNoLiquibaseTableAvailable() {
        assertEquals(-1, fixture.getCurrentTag(schema));
    }

    @Test
    public void testGetCurrentTagNoTagAvailable() {

        int targetTag = 1;
        fixture.upgrade(schema, targetTag);
        jdbcTemplate.execute("delete from BLA.DATABASECHANGELOG");

        assertEquals(-1, fixture.getCurrentTag(schema));
    }

    @Test(expected = NumberFormatException.class)
    public void GetCurrentTagFailedWrongEndTag() {

        int targetTag = 5;
        fixture.upgrade(schema, targetTag);
        fixture.getCurrentTag(schema);
    }

    @Test
    public void testUpgrade() {

        int targetTag = 2;
        fixture.upgrade(schema, targetTag);
        assertEquals(targetTag, fixture.getCurrentTag(schema));
    }

    @Test
    public void testUpgradeFailedWrongChangelog() {

        fixture.setChangelogFilename("oops");

        try {
            fixture.upgrade(schema, 3);
            fail();
        }
        catch (ErironLiquibaseUpdateException due) {
            assertEquals("Unable to upgrade BLA to tag 3", due.getMessage());
        }
    }

    @Test
    public void testDowngrade() {

        int setupTag = 3;
        fixture.upgrade(schema, setupTag);
        assertEquals(setupTag, fixture.getCurrentTag(schema));

        int targetTag = 2;
        fixture.downgrade(schema, targetTag);
        assertEquals(targetTag, fixture.getCurrentTag(schema));
    }

    @Test
    public void testDowngradeFailedWrongChangelog() {

        fixture.setChangelogFilename("bla");

        try {
            fixture.downgrade(schema,1);
            fail();
        }
        catch (ErironLiquibaseUpdateException due) {
            assertEquals("Unable to downgrade BLA to tag 1", due.getMessage());
        }
    }
}