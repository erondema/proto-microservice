package nl.eriron.liquibase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Erik Rondema
 */
public class ErironLiquibaseExecutorTest {

    private static final int EXPECTED_TAG = 4;
    private static final String SCHEMA = "BLA";
    private static final String CONFIRMATION_DIR = "target";
    private static final String CONFIRMATION_FILENAME = "downgrade_to_4.txt";

    private ErironLiquibaseExecutor fixture;
    private ErironLiquibaseDao erironLiquibaseDao;

    @Before
    public void setup() throws IOException {

        Path path = FileSystems.getDefault().getPath(CONFIRMATION_DIR, CONFIRMATION_FILENAME);
        Files.deleteIfExists(path);

        erironLiquibaseDao = mock(ErironLiquibaseDao.class);
        ErironLiquibaseProperties properties = new ErironLiquibaseProperties();
        properties.setConfirmationDirectory(CONFIRMATION_DIR);

        ErironLiquibaseConfiguration erironLiquibaseConfiguration = new ErironLiquibaseConfiguration();
        fixture = erironLiquibaseConfiguration.erironLiquibaseExecutor(erironLiquibaseDao, properties);
    }

    @Test
    public void testPostConstructAnnotation() throws NoSuchMethodException {

        Method method = fixture.getClass().getMethod("updateSchema");
        Annotation annotation = method.getAnnotation(PostConstruct.class);
        assertNotNull(annotation);
    }

    @Test(expected = ErironLiquibaseUpdateException.class)
    public void testUpdateDatabaseNoExpectedDatabaseTagSet() {

        ErironLiquibaseExecutor.setSchema(SCHEMA);
        fixture.updateSchema();
    }

    @Test(expected = ErironLiquibaseUpdateException.class)
    public void testUpdateDatabaseNoSchemaSet() {

        ErironLiquibaseExecutor.setExpectedDatabaseTag(EXPECTED_TAG);
        fixture.updateSchema();
    }

    @Test
    public void testUpdateDatabaseNotingToDo() {

        ErironLiquibaseExecutor.setExpectedDatabaseTag(EXPECTED_TAG);
        ErironLiquibaseExecutor.setSchema(SCHEMA);
        List<Integer> returnValues = Collections.singletonList(4);
        createMockBehavior(returnValues);

        fixture.updateSchema();

        verify(erironLiquibaseDao, times(0)).upgrade(anyString(), anyInt());
        verify(erironLiquibaseDao, times(0)).downgrade(anyString(), anyInt());
    }

    @Test
    public void testUpdateDatabaseUpgrade() {

        ErironLiquibaseExecutor.setExpectedDatabaseTag(EXPECTED_TAG);
        ErironLiquibaseExecutor.setSchema(SCHEMA);
        List<Integer> returnValues = Arrays.asList(3,4);
        createMockBehavior(returnValues);

        fixture.updateSchema();

        ArgumentCaptor<String> schemaCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> targetTagCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(erironLiquibaseDao, times(1)).upgrade(schemaCaptor.capture(), targetTagCaptor.capture());
        verify(erironLiquibaseDao, times(0)).downgrade(anyString(), anyInt());
        assertEquals(EXPECTED_TAG, targetTagCaptor.getValue().intValue());
        assertEquals(SCHEMA, schemaCaptor.getValue());
    }

    @Test
    public void testUpdateDatabaseDowngrade() throws IOException {

        ErironLiquibaseExecutor.setExpectedDatabaseTag(EXPECTED_TAG);
        ErironLiquibaseExecutor.setSchema(SCHEMA);
        Path path = FileSystems.getDefault().getPath(CONFIRMATION_DIR, CONFIRMATION_FILENAME);
        Files.createFile(path);

        List<Integer> returnValues = Arrays.asList(5,4);
        createMockBehavior(returnValues);

        fixture.updateSchema();

        ArgumentCaptor<String> schemaCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> targetTagCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(erironLiquibaseDao, times(0)).upgrade(anyString(), anyInt());
        verify(erironLiquibaseDao, times(1)).downgrade(schemaCaptor.capture(), targetTagCaptor.capture());
        assertEquals(SCHEMA, schemaCaptor.getValue());
        assertEquals(EXPECTED_TAG, targetTagCaptor.getValue().intValue());
    }

    @Test
    public void testUpdateDatabaseDowngradeFailedNoConfirmationFile() {

        ErironLiquibaseExecutor.setExpectedDatabaseTag(EXPECTED_TAG);
        ErironLiquibaseExecutor.setSchema(SCHEMA);
        List<Integer> returnValues = Arrays.asList(5,4);
        createMockBehavior(returnValues);

        try {
            fixture.updateSchema();
            fail();
        }
        catch(ErironLiquibaseUpdateException due) {
            String expected =
                "Database downgrade failed, because no confirmation file was found.\n" +
                "If you want to downgrade the database to tag 4, then add the following empty file target" +
                File.separator +
                "downgrade_to_4.txt and start the application.";

            assertEquals(expected, due.getMessage());
        }

        verify(erironLiquibaseDao, times(0)).upgrade(anyString(), anyInt());
        verify(erironLiquibaseDao, times(0)).downgrade(anyString(), anyInt());
    }

    @Test
    public void testUpdateDatabaseUpgradeFailedWrongTagNumber() {

        ErironLiquibaseExecutor.setExpectedDatabaseTag(EXPECTED_TAG);
        ErironLiquibaseExecutor.setSchema(SCHEMA);
        List<Integer> returnValues = Arrays.asList(3,3);
        createMockBehavior(returnValues);

        try {
            fixture.updateSchema();
            fail();
        }
        catch(ErironLiquibaseUpdateException due) {
            assertEquals("Update of database failed. Target tag was 4, but new tag is 3", due.getMessage());
        }

        ArgumentCaptor<String> schemaCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> targetTagCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(erironLiquibaseDao, times(1)).upgrade(schemaCaptor.capture(), targetTagCaptor.capture());
        verify(erironLiquibaseDao, times(0)).downgrade(anyString(), anyInt());
        assertEquals(SCHEMA, schemaCaptor.getValue());
        assertEquals(EXPECTED_TAG, targetTagCaptor.getValue().intValue());
    }

    @Test
    public void testUpdateDatabaseDowngradeFailedWrongTagNumber() throws IOException {

        ErironLiquibaseExecutor.setExpectedDatabaseTag(EXPECTED_TAG);
        ErironLiquibaseExecutor.setSchema(SCHEMA);
        Path path = FileSystems.getDefault().getPath(CONFIRMATION_DIR, CONFIRMATION_FILENAME);
        Files.createFile(path);

        List<Integer> returnValues = Arrays.asList(5,5);
        createMockBehavior(returnValues);

        try {
            fixture.updateSchema();
            fail();
        }
        catch(ErironLiquibaseUpdateException due) {
            assertEquals("Update of database failed. Target tag was 4, but new tag is 5", due.getMessage());
        }

        ArgumentCaptor<String> schemaCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> targetTagCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(erironLiquibaseDao, times(0)).upgrade(anyString(), anyInt());
        verify(erironLiquibaseDao, times(1)).downgrade(schemaCaptor.capture(), targetTagCaptor.capture());
        assertEquals(EXPECTED_TAG, targetTagCaptor.getValue().intValue());
        assertEquals(SCHEMA, schemaCaptor.getValue());
    }

    private void createMockBehavior(final List<Integer> returnValues) {

        when(erironLiquibaseDao.getCurrentTag(SCHEMA)).then(new Answer<Integer>() {

            int index = 0;

            @Override
            public Integer answer(InvocationOnMock invocationOnMock) {

                if(index >= returnValues.size()) {
                    fail("Too many calls to getCurrentTag()");
                }
                Integer value = returnValues.get(index);
                index++;
                return value;
            }
        });
    }
}