package nl.eriron.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * This class contains the properies that start with "common" and general settings of all microservices.
 *
 * @author Erik Rondema
 */
@Configuration
@ConfigurationProperties(value = "common")
public class CommonProperties {

    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
