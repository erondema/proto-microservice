package nl.eriron.common.input_layer;

import java.util.ArrayList;
import java.util.List;

/**
 * This class contains the error information that will be sent back that the caller, when a request cannot be
 * executed.
 * The following information is sent to the caller:
 * <ul>
 *     <li>timestamp - the moment when an error response is created</li>
 *     <li>status - a description of the status (e.g. bad request)</li>
 *     <li>message - a general message about the error that occured</li>
 *     <li>list of error details</li>
 * </ul>
 *
 * The error details contain the following information:
 * <ul>
 *     <li>objectName - the name of the object that (e.g. user object) is causing the problem</li>
 *     <li>field - the name of the field that is causing the problem</li>
 *     <li>message - the error message</li>
 * </ul>
 *
 * @author Erik Rondema
 */
public class ErrorResponse {

    private long timestamp;
    private String status;
    private String message;
    private List<Error> errors;

    /**
     * Constructor.
     *
     * @param status status description
     * @param message a general error message
     */
    public ErrorResponse(String status, String message) {

        timestamp = System.currentTimeMillis();
        this.status = status;
        this.message = message;
        errors = new ArrayList<>();
    }

    public void addError(String objectName, String field, String errorMessage) {

        errors.add(new Error(objectName, field, errorMessage));

        if (message != null) {
            message += "\n";
        }
        message = "Object: '" + objectName + "' Field: '" + field + "' Message: " + errorMessage;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<Error> getErrors() {
        return errors;
    }

    class Error {

        private String objectName;
        private String field;
        private String message;

        private Error(String objectName, String field, String message) {
            this.objectName = objectName;
            this.field = field;
            this.message = message;
        }

        public String getObjectName() {
            return objectName;
        }

        public String getField() {
            return field;
        }

        public String getMessage() {
            return message;
        }
    }
}