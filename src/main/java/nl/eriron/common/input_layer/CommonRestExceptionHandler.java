package nl.eriron.common.input_layer;

import nl.eriron.common.exception.EntityNotFoundException;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Locale;

/**
 * This handler is responsible for converting common exceptions into an {@link ErrorResponse} object.
 *
 * @author Erik Rondema
 */
@RestControllerAdvice
public class CommonRestExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(CommonRestExceptionHandler.class);

    private MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * Handle the exception that is thrown when an argument to a REST controller is not valid.
     *
     * @param exception the exception that is thrown
     * @return the error response
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleNotValid(MethodArgumentNotValidException exception) {

        ErrorResponse response = new ErrorResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(), null);

        BindingResult bindingResult = exception.getBindingResult();
        String objectName = bindingResult.getObjectName();
        for (ObjectError error: bindingResult.getGlobalErrors()) {
            String errorCode = error.getCode();
            Object[] arguments = error.getArguments();
            response.addError(objectName, null, messageSource.getMessage(errorCode, arguments, getLocale()));
        }

        for (FieldError error: bindingResult.getFieldErrors()) {
            String errorCode = error.getCode();
            Object[] arguments = error.getArguments();
            String field = error.getField();
            response.addError(objectName, field, messageSource.getMessage(errorCode, arguments, getLocale()));
        }

        LOG.info("handleNotValid: {}", response.getMessage());
        return response;
    }

    /**
     * Handle the exception that is thrown when an entity is not found.
     *
     * @param exception the exception that is thrown
     * @return the error response
     */
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleEntityNotFound(EntityNotFoundException exception) {

        String entity = exception.getEntityName();
        Object[] arguments = ArrayUtils.insert(0, exception.getArguments(), String.valueOf(exception.getId()));
        String message = messageSource.getMessage(entity + ".not.found", arguments, getLocale());
        LOG.info("handleEntityNotFound: {}", message);
        return new ErrorResponse(HttpStatus.NOT_FOUND.getReasonPhrase(), message);
    }

    @ExceptionHandler(ObjectOptimisticLockingFailureException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public ErrorResponse handleOptimisticLockingError(ObjectOptimisticLockingFailureException exception) {

        String fullClassName = exception.getPersistentClassName();
        String entity = getSimpleClassName(fullClassName);
        Object id = exception.getIdentifier();

        Object[] arguments = new Object[]{ String.valueOf(id) };
        String message = messageSource.getMessage(entity + ".changed", arguments, getLocale());
        LOG.info("handleOptimisticLockingError: {}", message);
        return new ErrorResponse(HttpStatus.PRECONDITION_FAILED.getReasonPhrase(), message);
    }

    /**
     * Handle all other (unexpected) exceptions.
     *
     * @param exception the exception that is thrown
     * @return the error response
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleException(Exception exception) {

        LOG.error("Unexpected exception", exception);
        String message = messageSource.getMessage("server.error", null, getLocale());
        return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), message);
    }

    private Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }

    private String getSimpleClassName(String className) {

        if (className != null) {
            int index = className.lastIndexOf('.');
            return className.substring(index + 1).toLowerCase();
        }
        else {
            return "";
        }
    }
}
