package nl.eriron.common.input_layer;

import nl.eriron.common.CommonProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import java.util.Locale;

/**
 * This class is responsible for creating the common Spring beans that are used in the input layer.
 *
 * @author Erik Rondema
 */
@Configuration
public class CommonInputLayerConfiguration {

    @Bean
    public LocaleResolver localeResolver(CommonProperties properties) {

        CookieLocaleResolver resolver = new CookieLocaleResolver();
        resolver.setDefaultLocale(new Locale(properties.getLanguage()));
        resolver.setCookieName("localeInfo");
        return resolver;
    }

    @Bean
    public MessageSource messageSource() {

        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("file:config/messages", "classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setCacheSeconds(30);
        return messageSource;
    }
}
