package nl.eriron.common.exception;

/**
 * This exception is thrown when an entity is not found when searching for the entity with its ID.
 *
 * @author Erik Rondema
 */
public class EntityNotFoundException extends RuntimeException {

    private final Long id;
    private final String entityName;
    private final transient Object[] arguments;

    /**
     * Constructor.
     *
     * @param id the ID that is used to lookup the entity
     * @param entityName the name of the entity that is not found
     * @param arguments additional information to pass
     */
    public EntityNotFoundException(Long id, String entityName, Object... arguments) {

        super();
        this.id = id;
        this.entityName = entityName;
        this.arguments = arguments;
    }

    public Long getId() {
        return id;
    }

    public String getEntityName() {
        return entityName;
    }

    public Object[] getArguments() {
        return arguments;
    }
}
