package nl.eriron.liquibase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * This class is responsible for executing the Liquibase upgrade or downgrade on a specific schema.
 *
 * When this class is instantiated as a Spring bean, then Spring will call {@link #updateSchema()} after all
 * setters have been called. It will then automatically determine if the schema must be upgraded, downgraded
 * or is up to date.
 * For a downgrade the executor checks if there is a confirmation file that it tries to delete,
 * if there is no file or it cannot be delete then the downgrade will fail.
 *
 * Before this class is instantiated, the static methods {@link #setExpectedDatabaseTag(int)} and
 * {@link #setSchema(String)} must be called.
 * This will specify which tag the schema must have for the software to run properly.
 *
 * @author Erik Rondema
 */
public class ErironLiquibaseExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErironLiquibaseExecutor.class);
    private static Integer expectedDatabaseTag;
    private static String schema;

    private String confirmationDirectory;
    private ErironLiquibaseDao erironLiquibaseDao;

    /**
     * Set the tag that the application expects.
     *
     * @param expectedDatabaseTag the tag that the application expects
     */
    public static void setExpectedDatabaseTag(int expectedDatabaseTag) {
        ErironLiquibaseExecutor.expectedDatabaseTag = expectedDatabaseTag;
    }

    /**
     * Set the schema that needs to be upgraded or downgraded.
     *
     * @param schema the name of the schema
     */
    public static void setSchema(String schema) {
        ErironLiquibaseExecutor.schema = schema;
    }

    @Required
    public void setConfirmationDirectory(String confirmationDirectory) {
        this.confirmationDirectory = confirmationDirectory;
    }

    @Required
    public void setErironLiquibaseDao(ErironLiquibaseDao erironLiquibaseDao) {
        this.erironLiquibaseDao = erironLiquibaseDao;
    }

    /**
     * Update the database to the tag that is specified in {@link #expectedDatabaseTag}.
     */
    @PostConstruct
    public void updateSchema() {

        if (expectedDatabaseTag == null) {
            throw new ErironLiquibaseUpdateException("Application did not specify the expected database tag.");
        }
        if (schema == null) {
            throw new ErironLiquibaseUpdateException("Application did not specify the schema.");
        }

        int targetTag = expectedDatabaseTag;

        int currentTag = erironLiquibaseDao.getCurrentTag(schema);
        if (currentTag == targetTag) {
            LOGGER.info("Database tag is {}. It is up-to-date.", currentTag);
        }
        else if (currentTag < targetTag) {
            LOGGER.info("Database tag is {}. It will be upgraded to {}", currentTag, targetTag);

            erironLiquibaseDao.upgrade(schema, targetTag);
            Integer newTag = erironLiquibaseDao.getCurrentTag(schema);
            checkUpdate(targetTag, newTag);

            LOGGER.info("Database is upgraded, new tag is {}", newTag);
        }
        else {
            LOGGER.info("Database tag is {}. It will be downgraded to {}", currentTag, targetTag);

            checkConfirmationFile(targetTag);
            erironLiquibaseDao.downgrade(schema, targetTag);
            Integer newTag = erironLiquibaseDao.getCurrentTag(schema);
            checkUpdate(targetTag, newTag);

            LOGGER.info("Database is downgraded, new tag is {}", newTag);
        }
    }

    private void checkUpdate(Integer targetTag, Integer newTag) {

        if (!targetTag.equals(newTag)) {
            String message = "Update of database failed. Target tag was %d, but new tag is %d";
            throw new ErironLiquibaseUpdateException(String.format(message, targetTag, newTag));
        }
    }

    private void checkConfirmationFile(int targetTag) {

        String filename = "downgrade_to_%d.txt";
        Path path = FileSystems.getDefault().getPath(confirmationDirectory, String.format(filename, targetTag));

        try {
            if (!Files.deleteIfExists(path)) {
                String messageTemplate =
                        "Database downgrade failed, because no confirmation file was found.\n" +
                        "If you want to downgrade the database to tag %d, then add the following empty file %s " +
                        "and start the application.";
                String message = String.format(messageTemplate, targetTag, path);

                LOGGER.error(message);
                throw new ErironLiquibaseUpdateException(message);
            }
        }
        catch (IOException ioe) {
            String messageTemplate = "Database downgrade failed, because the confirmation file %s could not deleted.";
            String message = String.format(messageTemplate, path.getFileName());

            LOGGER.error(message, ioe);
            throw new ErironLiquibaseUpdateException(message, ioe);
        }
    }
}
