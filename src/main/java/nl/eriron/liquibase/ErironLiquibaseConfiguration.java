package nl.eriron.liquibase;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * This class is responsible for defining all Spring beans.
 *
 * @author Erik Rondema
 */
@Configuration
public class ErironLiquibaseConfiguration {

    @Bean
    public ErironLiquibaseExecutor erironLiquibaseExecutor(ErironLiquibaseDao erironLiquibaseDao,
                                                           ErironLiquibaseProperties properties) {

        ErironLiquibaseExecutor executor = new ErironLiquibaseExecutor();
        executor.setErironLiquibaseDao(erironLiquibaseDao);
        executor.setConfirmationDirectory(properties.getConfirmationDirectory());

        return executor;
    }

    @Bean
    public ErironLiquibaseDao erironLiquibaseDao(@Qualifier("ErironLiquibaseJdbcTemplate") JdbcTemplate jdbcTemplate,
                                                 ErironLiquibaseProperties properties) {

        ErironLiquibaseDao erironLiquibaseDao = new ErironLiquibaseDao();
        erironLiquibaseDao.setJdbcTemplate(jdbcTemplate);
        erironLiquibaseDao.setChangelogFilename(properties.getChangelogFilename());
        return erironLiquibaseDao;
    }

    @Bean(name = "ErironLiquibaseJdbcTemplate")
    public JdbcTemplate jdbcTemplate(@Qualifier("ErironLiquibaseDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "ErironLiquibaseDataSource")
    public DataSource dataSource(ErironLiquibaseProperties properties) {

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(properties.getDatabaseUrl());

        if (properties.getDriverClassName() != null) {
            dataSource.setDriverClassName(properties.getDriverClassName());
        }

        dataSource.setUsername(properties.getDatabaseUsername());
        dataSource.setPassword(properties.getDatabasePassword());
        return dataSource;
    }
}
