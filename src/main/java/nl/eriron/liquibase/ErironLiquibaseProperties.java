package nl.eriron.liquibase;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * This class contains the properties that start with "eriron.liquibase" from the application.properties file.
 * These properties configure the upgrade and downgrade of the application with Liquibase.
 *
 * @author Erik Rondema
 */
@Configuration
@ConfigurationProperties(value = "eriron.liquibase")
public class ErironLiquibaseProperties {

    private String databaseUrl;
    private String driverClassName;
    private String databaseUsername;
    private String databasePassword;
    private String changelogFilename;
    private String confirmationDirectory;

    public String getDatabaseUrl() {
        return databaseUrl;
    }

    public void setDatabaseUrl(String databaseUrl) {
        this.databaseUrl = databaseUrl;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getDatabaseUsername() {
        return databaseUsername;
    }

    public void setDatabaseUsername(String databaseUsername) {
        this.databaseUsername = databaseUsername;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }

    public void setDatabasePassword(String databasePassword) {
        this.databasePassword = databasePassword;
    }

    public String getChangelogFilename() {
        return changelogFilename;
    }

    public void setChangelogFilename(String changelogFilename) {
        this.changelogFilename = changelogFilename;
    }

    public String getConfirmationDirectory() {
        return confirmationDirectory;
    }

    public void setConfirmationDirectory(String confirmationDirectory) {
        this.confirmationDirectory = confirmationDirectory;
    }
}
