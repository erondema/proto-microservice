package nl.eriron.liquibase;

/**
 * This exception is thrown when there is a problem updating the database.
 *
 * @author Erik Rondema
 */
public class ErironLiquibaseUpdateException extends RuntimeException {

    /**
     * Constructor.
     *
     * @param message a message explaining the problem
     */
    public ErironLiquibaseUpdateException(String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param message a message explaining the problem
     * @param cause the cause of the problem
     */
    public ErironLiquibaseUpdateException(String message, Throwable cause) {
        super(message, cause);
    }
}
