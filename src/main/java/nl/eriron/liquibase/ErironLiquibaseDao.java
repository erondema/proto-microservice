package nl.eriron.liquibase;

import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.CompositeResourceAccessor;
import liquibase.resource.FileSystemResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * This class is responsible updating a schema.
 * An update is either an upgrade, which means changing the schema to a higher tag,
 * or a downgrade, which means changing the schema to a lower tag.
 *
 * @author Erik Rondema
 */
public class ErironLiquibaseDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErironLiquibaseDao.class);

    private JdbcTemplate jdbcTemplate;
    private String changelogFilename;

    @Required
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Required
    public void setChangelogFilename(String changelogFilename) {
        this.changelogFilename = changelogFilename;
    }

    /**
     * Get the current tag of the schema.
     *
     * @param schema the name of the schema
     * @return the current tag or -1 if no tag has been set yet
     */
    public int getCurrentTag(String schema) {

        String sql = "select TAG from " + schema + ".DATABASECHANGELOG " +
            "where TAG is not null " +
            "order by ORDEREXECUTED desc";

        try {
            List<String> tags = jdbcTemplate.queryForList(sql, String.class);

            if (tags.isEmpty()) {
                return -1;
            }
            else {
                return parseTag(tags.get(0));
            }
        }
        catch(BadSqlGrammarException bsge) {
            LOGGER.debug("Exception thrown when try to get the current tag. " +
                        "We assume that the table does not exist yet", bsge);
            return -1;
        }
    }

    /**
     * Upgrade a schema to the given tag.
     *
     * @param schema the name of the schema
     * @param targetTag the target of the upgrade
     */
    public void upgrade(String schema, int targetTag) {

        try {
            String tag = String.valueOf(targetTag);
            update(schema, tag, true);
        }
        catch (SQLException | LiquibaseException e) {
            String message = String.format("Unable to upgrade %s to tag %d", schema, targetTag);
            LOGGER.error(message, e);
            throw new ErironLiquibaseUpdateException(message, e);
        }
    }

    /**
     * Downgrade a schema to the given tag.
     *
     * @param schema the name of the schema
     * @param targetTag the target of the downgrade
     */
    public void downgrade(String schema, int targetTag) {

        try {
            String tag = String.valueOf(targetTag);
            update(schema, tag, false);
        }
        catch (SQLException | LiquibaseException e) {
            String message = String.format("Unable to downgrade %s to tag %d", schema, targetTag);
            LOGGER.error(message, e);
            throw new ErironLiquibaseUpdateException(message, e);
        }
    }

    private void update(String schema, String targetTag, boolean upgrade) throws SQLException, LiquibaseException {

        Connection connection = jdbcTemplate.getDataSource().getConnection();
        JdbcConnection jdbcConnection = new JdbcConnection(connection);
        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        database.setLiquibaseSchemaName(schema);

        FileSystemResourceAccessor fileSystemResourceAccessor = new FileSystemResourceAccessor();
        ClassLoaderResourceAccessor classLoaderResourceAccessor = new ClassLoaderResourceAccessor();
        CompositeResourceAccessor resourceAccessor = new CompositeResourceAccessor(fileSystemResourceAccessor,
                                                                                   classLoaderResourceAccessor);
        Liquibase liquibase = new Liquibase(changelogFilename, resourceAccessor, database);

        if (upgrade) {
            liquibase.update(targetTag, new Contexts());
        }
        else {
            liquibase.rollback(targetTag, new Contexts());

            // Unfortunately the developers of Liquibase made a change to the downgrade functionality in
            // version 3.5.2 and since then the downgrade does not function as expected
            // (see https://stackoverflow.com/questions/40263564/liquibase-rollback-to-tag-mechanism-also-deletes-tag-record-how-come,
            // https://liquibase.jira.com/browse/CORE-2946 and https://liquibase.jira.com/browse/CORE-2815)
            // A workaround for this problem is doing an upgrade with the same tag directly after a downgrade.
            liquibase.update(targetTag, new Contexts());
        }
    }

    private int parseTag(String tag) {

        try {
            return Integer.parseInt(tag);
        }
        catch (NumberFormatException nfe) {
            LOGGER.error("Found tag is not a number: {}", tag, nfe);
            throw nfe;
        }
    }
}
