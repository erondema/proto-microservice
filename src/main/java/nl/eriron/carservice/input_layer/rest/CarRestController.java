package nl.eriron.carservice.input_layer.rest;

import nl.eriron.carservice.dto.CarDto;
import nl.eriron.carservice.dto.NewCarDto;
import nl.eriron.carservice.dto.validator.CarDtoValidator;
import nl.eriron.carservice.service_layer.CarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * This REST controller is responsible for providing an endpoint to create, read, update and delete cars.
 * Cars are validated using {@link CarDtoValidator}.
 *
 * @author Erik Rondema
 */
@RestController
@RequestMapping("/cars")
public class CarRestController {

    private static final Logger LOG = LoggerFactory.getLogger(CarRestController.class);

    private CarService carService;

    @Autowired
    public void setCarService(CarService carService) {
        this.carService = carService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CarDto createCar(@RequestBody @Valid NewCarDto newCarDto) {

        LOG.info("createCar: {}", newCarDto);
        return carService.createCar(newCarDto);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateCar(@PathVariable Long id, @RequestBody @Valid CarDto carDto) {

        LOG.info("updateCar: {}", carDto);
        carService.updateCar(id, carDto);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCar(@PathVariable Long id) {

        LOG.info("deleteCar: {}", id);
        carService.deleteCar(id);
    }

    @GetMapping
    public List<CarDto> getCars() {

        if (LOG.isInfoEnabled()) {
            LOG.info("getCars");
        }
        return carService.getCars();
    }

    @GetMapping(params = { "text" })
    public List<CarDto> findCarsByBrandContaining(@RequestParam String text) {

        LOG.info("findCarsByBrandContaining: text={}", text);
        return carService.findCarsByBrandContaining(text);
    }
}
