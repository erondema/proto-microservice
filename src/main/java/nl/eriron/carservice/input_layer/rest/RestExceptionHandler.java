package nl.eriron.carservice.input_layer.rest;

import nl.eriron.common.input_layer.ErrorResponse;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Locale;
import java.util.Objects;

/**
 * This handler is responsible for converting exceptions into an {@link ErrorResponse} object.
 *
 * @author Erik Rondema
 */
@RestControllerAdvice
public class RestExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

    private MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * Handle a data integrity violation exception.
     * One exception is expected: A customerId is used that already exists.
     *
     * @param dive the exception containing information
     * @return the error response containing a locale specific message
     */
    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleDataIntegrityViolationException(DataIntegrityViolationException dive) {

        String code = null;
        Object[] arguments = null;

        if (dive.getCause() instanceof ConstraintViolationException) {

            ConstraintViolationException cve = (ConstraintViolationException) dive.getCause();
            if ("car_registration_number_key".equals(cve.getConstraintName())) {
                code = "car.registrationNumber.already.exists";
                arguments = new Object[]{ extractUsername(cve.getSQLException().getMessage())};
            }
        }

        String message = messageSource.getMessage(Objects.toString(code, "server.general.contraint.violation"),
                                                  arguments,
                                                  getLocale());
        LOG.info("handleDataIntegrityViolationException: {}", message);
        return new ErrorResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(), message);
    }

    private Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }

    private String extractUsername(String errorMessage) {

        final String searchKey1 = "(registration_number)=(";
        final String searchKey2 = ") already exists";

        int index1 = errorMessage.indexOf(searchKey1);
        int index2 = errorMessage.indexOf(searchKey2);

        if (index1 < 0 || index2 < 0 || index2 <= index1) {
            return null;
        }
        else {
            return errorMessage.substring(index1 + searchKey1.length(), index2);
        }
    }
}
