package nl.eriron.carservice.input_layer;

import nl.eriron.carservice.dto.validator.CarDtoValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;

/**
 * This class is responsible for creating the Spring beans that are used in the input layer.
 *
 * @author Erik Rondema
 */
@Configuration
public class InputLayerConfiguration {

    @Bean
    public CarDtoValidator carDtoValidator() {
        return new CarDtoValidator();
    }

    @Bean
    public ConfigurableWebBindingInitializer configurableWebBindingInitializer(CarDtoValidator carDtoValidator) {

        ConfigurableWebBindingInitializer initializer = new ConfigurableWebBindingInitializer();
        initializer.setValidator(carDtoValidator);
        return initializer;
    }
}
