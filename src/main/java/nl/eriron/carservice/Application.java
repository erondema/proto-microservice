package nl.eriron.carservice;

import nl.eriron.liquibase.ErironLiquibaseExecutor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.cloud.config.server.EnableConfigServer;

import java.util.TimeZone;

/**
 * This is the main class of this application.
 * This application is build with Spring-Boot.
 *
 * @author Erik Rondema
 */
@SpringBootApplication(scanBasePackages = "nl.eriron", exclude = LiquibaseAutoConfiguration.class)
@EnableConfigServer
public class Application {

    /**
     * Start the application.
     *
     * @param args command line parameters are not required
     */
    public static void main(String[] args) {

        ErironLiquibaseExecutor.setSchema("car_schema");
        ErironLiquibaseExecutor.setExpectedDatabaseTag(1);

        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(Application.class, args);
    }
}
