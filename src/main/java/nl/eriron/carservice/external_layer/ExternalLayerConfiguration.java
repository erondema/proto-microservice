package nl.eriron.carservice.external_layer;

import com.zaxxer.hikari.HikariDataSource;
import nl.eriron.carservice.external_layer.db.DatabaseProperties;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * This class is responsible for creating the Spring beans that are used in the external layer.
 *
 * @author Erik Rondema
 */
@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
@EntityScan(basePackages = { "nl.eriron.carservice.domain" })
public class ExternalLayerConfiguration {

    @Primary
    @Bean
    public DataSource dataSource(DatabaseProperties properties) {

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(properties.getDatabaseUrl());
        dataSource.setDriverClassName(properties.getDriverClassName());
        dataSource.setUsername(properties.getDatabaseUsername());
        dataSource.setPassword(properties.getDatabasePassword());
        return dataSource;
    }
}
