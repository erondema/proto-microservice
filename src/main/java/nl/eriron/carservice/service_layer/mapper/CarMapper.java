package nl.eriron.carservice.service_layer.mapper;

import nl.eriron.carservice.domain.Car;
import nl.eriron.carservice.dto.CarDto;
import nl.eriron.carservice.dto.NewCarDto;
import org.mapstruct.Mapper;

/**
 * This mapper is responsible for converting the following objects:
 * <ul>
 *     <li>{@link CarDto} into {@link Car}</li>
 *     <li>{@link Car} into {@link CarDto}</li>
 *     <li>{@link Car} into {@link NewCarDto}</li>
 * </ul>
 *
 * MapStruct will generate an implementation for this interface during compilation.
 *
 * @author Erik Rondema
 */
@Mapper
public interface CarMapper {

    /**
     * Convert a domain object into a DTO.
     *
     * @param car the domain object
     * @return the DTO
     */
    CarDto convert(Car car);

    /**
     * Convert a DTO into a domain object.
     *
     * @param carDto the DTO
     * @return the domain object
     */
    Car convert(CarDto carDto);

    /**
     * Convert a DTO into a domain object.
     *
     * @param newCarDto the DTO
     * @return the domain object
     */
    Car convert(NewCarDto newCarDto);
}
