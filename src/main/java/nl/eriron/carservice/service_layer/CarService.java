package nl.eriron.carservice.service_layer;

import nl.eriron.carservice.dto.CarDto;
import nl.eriron.carservice.dto.NewCarDto;

import java.util.List;

/**
 * Definition of the service that is responsible for the management of cars.
 *
 * @author Erik Rondema
 */
public interface CarService {

    /**
     * Create a new car.
     *
     * @param newCarDto DTO containing data for the new car
     * @return DTO containing all data of the car including an ID
     */
    CarDto createCar(NewCarDto newCarDto);

    /**
     * Update the given car.
     *
     * @param id the ID of an existing car
     * @param carDto DTO containing the data of the car
     */
    void updateCar(Long id, CarDto carDto);

    /**
     * Delete the car with the given id.
     *
     * @param id the ID of an existing car
     */
    void deleteCar(Long id);

    /**
     * Get all the cars.
     *
     * @return the cars
     */
    List<CarDto> getCars();

    /**
     * Find cars that contain the given text in the brand name of the car.
     *
     * @param text some text
     * @return the cars matching the criteria
     */
    List<CarDto> findCarsByBrandContaining(String text);
}
