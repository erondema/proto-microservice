package nl.eriron.carservice.service_layer.impl;

import nl.eriron.carservice.domain.Car;
import nl.eriron.carservice.dto.CarDto;
import nl.eriron.carservice.dto.NewCarDto;
import nl.eriron.carservice.external_layer.db.CarRepository;
import nl.eriron.carservice.service_layer.CarService;
import nl.eriron.carservice.service_layer.mapper.CarMapper;
import nl.eriron.common.exception.EntityNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Implementation of {@link CarService}.
 *
 * @author Erik Rondema
 */
@Transactional
public class CarServiceImpl implements CarService {

    private CarRepository carRepository;
    private CarMapper mapper;

    public CarServiceImpl(CarRepository carRepository, CarMapper mapper) {

        this.carRepository = carRepository;
        this.mapper = mapper;
    }

    @Override
    public CarDto createCar(NewCarDto newCarDto) {

        Car car = mapper.convert(newCarDto);
        car = carRepository.save(car);
        return mapper.convert(car);
    }

    @Override
    public void updateCar(Long id, CarDto carDto) {

        Optional<Car> result = carRepository.findById(id);
        if (!result.isPresent()) {
            throw new EntityNotFoundException(id, "car");
        }

        Car car = mapper.convert(carDto);
        car.setId(id);

        carRepository.save(car);
    }

    @Override
    public void deleteCar(Long id) {

        try {
            carRepository.deleteById(id);
        }
        catch (EmptyResultDataAccessException erdae) {
            throw new EntityNotFoundException(id, "car");
        }
    }

    @Override
    public List<CarDto> getCars() {

        Iterable<Car> cars = carRepository.findAll();
        return StreamSupport
                .stream(cars.spliterator(), false)
                .map(car -> this.mapper.convert(car))
                .collect(Collectors.toList());
    }

    @Override
    public List<CarDto> findCarsByBrandContaining(String text) {

        Iterable<Car> cars = carRepository.findByBrandContainingOrderByBrandAsc(text);
        return StreamSupport
                .stream(cars.spliterator(), false)
                .map(car -> this.mapper.convert(car))
                .collect(Collectors.toList());
    }
}
