package nl.eriron.carservice.service_layer;

import nl.eriron.carservice.external_layer.db.CarRepository;
import nl.eriron.carservice.service_layer.impl.CarServiceImpl;
import nl.eriron.carservice.service_layer.mapper.CarMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class is responsible for creating the Spring beans that are used in the service layer.
 *
 * @author Erik Rondema
 */
@Configuration
public class ServiceLayerConfiguration {

    @Bean
    public CarService carService(CarRepository carRepository, CarMapper carMapper) {
        return new CarServiceImpl(carRepository, carMapper);
    }

    @Bean
    public CarMapper carMapper() {
        return Mappers.getMapper(CarMapper.class);
    }
}
