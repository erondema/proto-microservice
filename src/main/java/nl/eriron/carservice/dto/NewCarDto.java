/*
 *
 * @Author Erik Rondema
 */
package nl.eriron.carservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * A data transfer object containing information about a new car.
 *
 * @author Erik Rondema
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewCarDto {

    private String registrationNumber;
    private String brand;
    private String type;
    private BigDecimal price;
}
