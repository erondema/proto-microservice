package nl.eriron.carservice.dto.validator;

import nl.eriron.carservice.dto.CarDto;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This class is responsible for validating if a {@link CarDto} object has valid content.
 *
 * @author Erik Rondema
 */
public class CarDtoValidator implements Validator {

    private static final int MAX_BRAND_LENGTH = 255;
    private static final int MAX_REGISTRATION_NUMBER_LENGTH = 30;

    @Override
    public boolean supports(Class<?> clazz) {
        return CarDto.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {

        CarDto dto = (CarDto) object;
        validateCustomerIdField(dto.getRegistrationNumber(), errors);
        validateBrandField(dto.getBrand(), errors);
        ValidationUtils.rejectIfEmpty(errors,
                                      "price",
                                      "car.price.empty",
                                      null,
                                      "price is empty");

        ValidationUtils.rejectIfEmpty(errors,
                                      "type",
                                      "car.type.empty",
                                      null,
                                      "type is empty");
    }

    private void validateCustomerIdField(String customerId, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,
                "registrationNumber",
                "car.registrationNumber.empty",
                null,
                "registrationNumber is empty");

        if (customerId != null && customerId.length() > MAX_REGISTRATION_NUMBER_LENGTH) {
            Object[] arguments = new Object[] {MAX_REGISTRATION_NUMBER_LENGTH};
            errors.rejectValue("registrationNumber",
                    "car.registrationNumber.too.long",
                    arguments,
                    "registrationNumber is too long");
        }
    }

    private void validateBrandField(String brand, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,
                                                  "brand",
                                                  "car.brand.empty",
                                                  null,
                                                  "brand is empty");

        if (brand != null && brand.length() > MAX_BRAND_LENGTH) {
            Object[] arguments = new Object[] {MAX_BRAND_LENGTH};
            errors.rejectValue("brand",
                               "car.brand.too.long",
                               arguments,
                               "brand is too long");
        }
    }
}
