Introduction
============
This is a prototype of a microservice.

IDE setup
=========
This project uses Maven (https://maven.apache.org/index.html) to build the software, so instruct your IDE to use maven
to configure your project. <br>
Because this project uses the Lombok project (https://projectlombok.org/) you need to configure your IDE to support
this project, otherwise your IDE will get compilation errors. See https://projectlombok.org/setup/overview for 
instructions for your IDE and additional information for Eclipse and Intellij at https://www.baeldung.com/lombok-ide.
<br>
This project also uses MapStruct (http://mapstruct.org/) and you can configure IDE support for this project as well
with instructions at http://mapstruct.org/documentation/ide-support/.

Database setup
==============
On your development machine the application will connect to a locally running database. The application uses a 
PostgreSQL database that can be run inside a Docker container. To start the database execute the following steps:

1. open a (Docker) terminal or console and go to the directory `<project-dir>/database/`
2. build a docker image with the command `docker build -t "microservice-db" .`
3. start the container with the command `docker run --name microservice-db --rm -d -p 15432:5432 -e POSTGRES_PASSWORD=welcome123 microservice-db`

The database is now running and you can make a connection to it with the following properties:
* `URL` - `jdbc:postgresql://localhost:15432/postgres`
* `username` - `postgres`
* `password` - `welcome123`

The following commands can be usefull:
* To stop the container execute: `docker stop microservice-db`
* To restart the container execute: `docker stop microservice-db; docker run --name microservice-db --rm -d -p 15432:5432 -e POSTGRES_PASSWORD=welcome123 microservice-db`

Liquibase
=========
The application will automatically update or rollback the database when it is starting up. It reads the specification of
the database objects from the files in the directory `<project-dir>/src/resources/changelogs/`.
If you want to update or rollback the database manually you must use Liquibase. Make sure you have installed Liquibase 
(https://download.liquibase.org/download/?frm=n) and downloaded a PostgreSQL driver 
(https://jdbc.postgresql.org/download.html). You can use the `liquibase.bat` file or `liquibase` bash script to execute 
commands.
Use the following command to update the database:
<pre>
liquibase 
    --changeLogFile=<path-to-changelog-file>\changelog-master.xml 
    --username=postgres 
    --password=welcome123 
    --url=jdbc:postgresql://localhost:5432/postgres 
    --driver=org.postgresql.Driver 
    --classpath=<path-to-driver>\postgresql-42.2.5.jar 
    --defaultSchemaName=car_schema 
    update
</pre>
See documentation for other commands (https://www.liquibase.org/documentation/command_line.html).

Running inside IDE
==================
This project is based on Spring Boot (http://projects.spring.io/spring-boot/) and an embedded Spring Cloud Config Server 
(https://cloud.spring.io/spring-cloud-config/multi/multi__spring_cloud_config_server.html). This means that when the
application is starting up it tries to find it's configuration files in a Git repository and to make this successful it
requires 3 additional properties that are not in the `bootstrap.properties` file. These 3 properties are left out of the
`bootstrap.properties` file on purpose, because they depend on the environment. Here are the 3 properties:

* `ENCRYPT_KEY` - this property must contain a key with the value taken from LastPass secure note `JavaDevelopment`. 
Make sure that you restart IntelliJ after changing your system environment. 
This encryption key is used to decrypt passwords that are stored encrypted in the configuration files. Put this 
property in your environment variables, because it is the same for all microservices you run on your development 
machine.

* `SPRING_PROFILES_ACTIVE` - this property must contain the value `DEV`. It tells the application that it is running on
a development machine. Put this property in your environment variables, because it is the same for all microservices
you run on your development machine.

* `spring.config.location` - this property must contain the location of the `bootstrap.properties` file which is 
`file:src/main/resources/bootstrap.properties`. Put this property in your startup configuration for the class 
`nl.eriron.carservice.Application` (e.g. VM options -Dspring.config.location=file:src/main/resources/bootstrap.properties)

To run or debug the application start the class `nl.eriron.carservice.Application` with the specified 
properties set.

Running as standalone application
=================================
To run the application outside of your IDE as a standalone application you have to execute the following steps:

1. create a directory outside of your project, for example `run-space`
2. create a sub-directory `config` inside `run-space`
3. copy the file `messages_nl.properties` and place it in the `config` directory
4. copy the file `bootstrap.properties` and place it in the `config` directory
6. add `ENCRYPT_KEY` and `SPRING_PROFILES_ACTIVE` to your environment variables as described in the previous paragraph
7. build a jar file with the command `mvn package`
8. copy the jar file `proto-microservice-<version-number>.jar` and place it in the `run-space` directory 
9. open a terminal or console and go to the `run-space` directory
10. start the application with the command `java -jar proto-microservice-<version-number>.jar`

The messages inside the `messages_nl.properties` can be modified without restarting the application. The cache time is
30 seconds, so after 30 seconds the modified messages will be used.

Maven
=====
POM file
--------
The `sortpom-maven-plugin` (https://github.com/Ekryd/sortpom) is used to keep the POM file neat. It will sort the 
dependencies and properties alphabetically and it will keep the formatting the same for each element.

Jacoco
------
Normally the `jacoco:report` (to generate a code coverage report) goal is executed during the test phase of maven. 
This has been disabled, because on a Windows machine it does not work in combination with a Data JPA Test. 
On the build server `jacoco:report` is executed as part of the `sonar:sonar` goal, this will make sure that we have a 
code coverage report in Sonar.

REST API
========
This application provides a REST API and the documentation for this API is generated with springdoc-openapi 
(https://springdoc.org/). When the application is running, you can view the documentation by visiting this 
URL: `http://<host>:<port>/carservice/swagger-ui.html`. For example
when running on your development machine navigate to http://localhost:8080/carservice/swagger-ui.html.

Spring actuators
================
This application provides access to Spring actuators 
(see https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html). These endpoints
can be visited with the following URL: `http://<host>:<port>carservice/actuator/<actuator-id>`. For example to
get an overview of the environment settings on your development machine navigate to 
http://localhost:8080/carservice/actuator/env.
To limit access to the actuator you can change the property `management.endpoints.web.exposure.include` in the 
`application.property` file.
