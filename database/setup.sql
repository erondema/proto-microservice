-- NOTE: This script should only be used on a development machine.
CREATE USER car_schema_owner WITH PASSWORD 's3cr3t';
CREATE USER car_service WITH PASSWORD 'g3h31m';
CREATE SCHEMA car_schema AUTHORIZATION car_schema_owner;
GRANT USAGE ON SCHEMA car_schema TO car_service;